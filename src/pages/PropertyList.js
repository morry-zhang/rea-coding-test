/**
 * Property list page
 * **/
import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import "./propertylist.css";
import getProperty from "../api/getProperty";
import PropertyCard from "./components/PropertyCard";

class PropertyList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            resultList: [], //results property list
            savedList: [],  //saved property list
        }
    }

    componentDidMount() {
        this.getPropertyList();
    }

    //get result and saved property list from api
    getPropertyList = () => {
        this.setState({
            resultList: getProperty.results,
            savedList: getProperty.saved
        })
    }

    //add selected property item from results property column to saved property column
    handleAdd = item => {
        let {savedList} = this.state;
        savedList.push(item);
        this.setState({savedList});
    }

    //remove selected property from saved property column by property id
    handleRemove = id => {
        let {savedList} = this.state;
        let filterList = savedList.filter(item => item.id !== id);
        this.setState({
            savedList: filterList
        });
    }

    render() {
        let {resultList, savedList} = this.state;

        return (
            <div className='property-list-container'>
                {/*Results property column*/}
                <div className='list-container'>
                    <h3 className='text-center'>Results</h3>
                    <div className='list-section'>
                        {resultList.map(item => {
                            return (
                                <PropertyCard
                                    key={item.id}
                                    item={item}
                                    type='result'
                                    handleAdd={this.handleAdd}
                                />
                            )
                        })}
                    </div>
                </div>

                {/*Saved property column*/}
                <div className='list-container'>
                    <h3 className='text-center'>Saved Properties</h3>
                    <div className='list-section'>
                        {savedList.map(item => {
                            return (
                                <PropertyCard
                                    key={item.id}
                                    item={item}
                                    type='saved'
                                    handleRemove={this.handleRemove}
                                />
                            )
                        })}
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(PropertyList);