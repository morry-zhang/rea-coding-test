/**
 * Property list page -> Property card
 * **/
import "./propertycard.css";
import {useState} from "react";

const PropertyCard = (props) => {
    const [isHover, setIsHover] = useState(false);//whether card is hovered
    const {item, type, handleAdd, handleRemove} = props;
    //item: a property in the property list
    //type: [result, saved], means card is showed in two type of columns
    //handleAdd: add selected property from results property column to saved property column
    //handleRemove: remove selected property from saved property column

    return (
        <div className='property-card'
             onMouseEnter={()=>setIsHover(true)}
             onMouseLeave={()=>setIsHover(false)}
        >
            {/*Card header*/}
            <div className='card-header'
                 style={{backgroundColor:
                         item.agency.brandingColors
                             ? item.agency.brandingColors.primary
                             : 'white'
                 }}
            >
                <img src={item.agency.logo}/>
            </div>

            {/*Card main image*/}
            <div className='card-image'>
                <img src={item.mainImage}/>
            </div>

            {/*Card price and add/remove button*/}
            <div className='card-price'>
                <p>{item.price}</p>
            </div>

            {/*add or remove button*/}
            <div className='text-center card-button-section'
                 style={{display: isHover ? "" : "none"}}
            >
                {type==='result' ? (
                    <button className='card-add-button'
                            onClick={()=>handleAdd(item)}
                    >
                        Add
                    </button>
                ) : (
                    <button className='card-remove-button'
                            onClick={()=>handleRemove(item.id)}
                    >
                        Remove
                    </button>
                )
                }
            </div>
        </div>
    )
}

export default PropertyCard;