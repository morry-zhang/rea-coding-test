import './App.css';
import {
    BrowserRouter as Router,
    Route,
    Switch
} from "react-router-dom";
import PropertyList from "./pages/PropertyList";

function App() {
  return (
    <div className="App">
        <Router>
            <Switch>
                <Route exact path="/" component={PropertyList}/>
            </Switch>
        </Router>
    </div>
  );
}

export default App;
